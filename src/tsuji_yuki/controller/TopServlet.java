package tsuji_yuki.controller;

import java.io.IOException;
import java.util.Calendar;
//import java.util.List;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tsuji_yuki.beans.User;
import tsuji_yuki.beans.UserComment;
import tsuji_yuki.beans.UserMessage;
import tsuji_yuki.service.CommentService;
import tsuji_yuki.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		User user = (User) request.getSession().getAttribute("loginUser");

		Calendar calendar = Calendar.getInstance();
		int year        = calendar.get(Calendar.YEAR);
		int month       = calendar.get(Calendar.MONTH);
		int day         = calendar.get(Calendar.DAY_OF_MONTH);
		String nowTime = year + "/" + month + "/" + day;

		request.setAttribute("oldTimeSearch" , nowTime);
		request.setAttribute("newTimeSearch" , nowTime);

		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		String categorySearch = request.getParameter("categorySearch");
		String oldTimeSearch = request.getParameter("oldTimeSearch");
		String newTimeSearch = request.getParameter("newTimeSearch");

		List<UserMessage> messages = new MessageService().getMessage(categorySearch,oldTimeSearch,newTimeSearch);
		List<UserComment> comments = new CommentService().getComment();


		request.setAttribute("categorySearch" , categorySearch);
		request.setAttribute("oldTimeSearch" , oldTimeSearch);
		request.setAttribute("newTimeSearch" , newTimeSearch);

		request.setAttribute("comments" , comments);
		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}
