<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./CSS/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrapw">
<div class="main-contents">
<div class="background">
<div class="top-title">
<p><b>| 掲示板システム</b></p>
</div>
<c:if test="${ not empty errorMessages }">
	<div class="errormessage">
	<b>ERROR !!</b>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>


<form action="login" method="post"><br />

	<label for="login">ログインID　</label>
	<input name="loginId" value = "${loginId}"  id="loginId"/> <br />
	<br />
	<label for="password">パスワード　</label>
	<input name="password" type="password" id="password"/> <br />
	<br />
	<input class="comment_btn" type="submit" value="ログイン" /> <br />

</form>
<div class="copyright"><p>Copyright(c)Tsuji Yuki</p></div>
</div>
</div>
</div>
</body>
</html>
