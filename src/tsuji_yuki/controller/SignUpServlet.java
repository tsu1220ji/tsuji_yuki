package tsuji_yuki.controller;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.Branch;
import tsuji_yuki.beans.Position;
import tsuji_yuki.beans.User;
import tsuji_yuki.service.BranchService;
import tsuji_yuki.service.PositionService;
import tsuji_yuki.service.UserService;

@WebServlet(urlPatterns ={ "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();

       	List<Branch> branches = new BranchService().getBranches();
    	List<Position> positions = new PositionService().getPositions();
    	session.setAttribute("branches" , branches);
    	session.setAttribute("positions" , positions);

    	request.getRequestDispatcher("signup.jsp").forward(request,response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    	request.setCharacterEncoding("UTF-8");

      	List<Branch> branches = new BranchService().getBranches();
    	List<Position> positions = new PositionService().getPositions();
    	request.setAttribute("branches" , branches);
    	request.setAttribute("positions" , positions);


		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setName(request.getParameter("name"));
		user.setPosition(request.getParameter("position"));
		user.setBranch(request.getParameter("branch"));
		user.setPassword(request.getParameter("password"));
		user.setIsDeleted(0);

		List<String>userIdList = new ArrayList<String>();
		new UserService().idSelect(userIdList);

		for(int i=0; i < userIdList.size(); i++){
			if( userIdList.get(i).equals(request.getParameter("loginId")) ){

				List<String> messages = new ArrayList<String>();
				messages.add("ログインIDが重複しています");
				request.setAttribute("user",user);
				request.setAttribute("errorMessages",messages);
				request.setAttribute("positionsId", request.getParameter("position"));
				request.setAttribute("branchesId", request.getParameter("branch"));
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("signup.jsp").forward(request, response);
			}
		}
		List<String> messages = new ArrayList<String>();
		if (isValid(request,messages)){
			new UserService().register(user);
			response.sendRedirect("./management");
		}else{
			request.setAttribute("user",user);
			request.setAttribute("errorMessages",messages);
			request.setAttribute("positionsId", request.getParameter("position"));
			request.setAttribute("branchesId", request.getParameter("branch"));
			request.getRequestDispatcher("signup.jsp").forward(request,response);
		}
	}

    private boolean isValid(HttpServletRequest request,List<String>messages){

    	String loginId = request.getParameter("loginId");
    	String password = request.getParameter("password");
		String name = request.getParameter("name");
		String position = request.getParameter("position");
		String branch = request.getParameter("branch");
		String rePassword = request.getParameter("rePassword");


		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(StringUtils.length(loginId) > 0){
				if(StringUtils.length(loginId) < 6  ){
	    		messages.add("ログインIDを6文字以上にしてください");
	    	}
	    	if(StringUtils.length(loginId) > 20 ){
	    		messages.add("ログインIDを20文字以下にしてください");
	    	}
	    	if(StringUtils.isAsciiPrintable(loginId) == false){
	    		messages.add("ログインIDを半角英数字にしてください");
	    	}
		}

    	if (StringUtils.isEmpty(password)){
    		messages.add("パスワードを入力してください");
    	}
    	if (StringUtils.isEmpty(rePassword)){
    		messages.add("確認用パスワードを入力してください");
    	}

    	if(StringUtils.length(password) > 0){
	    	if(StringUtils.length(password) < 6 ){
	    		messages.add("パスワードを6文字以上にしてください");
	    	}
	    	if(StringUtils.length(password) > 20 ){
	    		messages.add("パスワードを20文字以下にしてください");
	    	}
    	}

    	if (StringUtils.isEmpty(name)){
    		messages.add("名前を入力してください");
    	}

    	if (StringUtils.isEmpty(position)){
    		messages.add("部署・役職を選択してください");
    	}
    	if (StringUtils.isEmpty(branch)){
    		messages.add("支店を選択してください");
    	}
    	if(StringUtils.length(rePassword) >= 6 &&  StringUtils.length(password) >= 6){
	    	if(!(password.equals(rePassword))){
				messages.add("パスワードと確認用パスワードがあってません");
			}
    	}
    	if (messages.size() == 0){
    		return true;
    	}else{
    		return false;
    	}
    }
}
