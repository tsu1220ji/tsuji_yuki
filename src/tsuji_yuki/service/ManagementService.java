package tsuji_yuki.service;

import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tsuji_yuki.beans.User;
import tsuji_yuki.dao.ManagementDao;



public class ManagementService {
	private static final int LIMIT_NUM = 1000;

	public List<User> getUserList() {

		Connection connection = null;
		try {
			connection = getConnection();

			ManagementDao userListDao = new ManagementDao();
			List<User> ret = userListDao.getUserList(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}

}
