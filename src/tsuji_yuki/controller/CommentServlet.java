package tsuji_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.Comment;
import tsuji_yuki.beans.User;
import tsuji_yuki.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();

			comment.setText(request.getParameter("comment"));
			comment.setUserId(user.getId());

			int id = Integer.parseInt(request.getParameter("id"));
			comment.setMessageId(id);

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {

			session.setAttribute("messageIdComment", request.getParameter("id"));
			session.setAttribute("errorMessages", messages);
			session.setAttribute("commentText",request.getParameter("comment"));

			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true) {
			messages.add("コメントを入力してください");
		}

		if (500 < comment.length()) {
			messages.add("500文字以下で入力してください");
		}

		if(StringUtils.isBlank(comment)){
			messages.add("コメントが空白です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

