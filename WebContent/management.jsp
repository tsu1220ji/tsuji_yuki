<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー画面</title>
	<link href="./CSS/style.css" rel="stylesheet" type="text/css" >
</head>
<body>
<div id="wrap">
<div class="main-contents">
<a href="./" class="square_btn">ホーム</a>
<a href="signup" class="square_btn">ユーザー新規登録</a>
<br/>
<br/>
<c:if test="${ not empty errorMessages }">
	<div class="errormessage">
	<b>ERROR !!</b>
		<ul>
			<c:forEach items="${errorMessages }" var="messages">
				<li><c:out value="${messages}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<div class="title">
	<p>ユーザー管理</p>
</div>


<table>
<thead>
  <tr>
    <th>　　　ログインID　　　</th>
    <th>　　　　名前　　　　</th>
    <th>　　　支店　　　</th>
    <th>　　役職　　</th>
    <th>停止・復活</th>
    <th>　編集　</th>
  </tr>
</thead>

<tbody>
<c:forEach var="userList" items="${userList}">
	  <tr>
	    <td>${userList.loginId }</td>
	    <td>${userList.name}</td>

		<c:forEach items="${branches}" var="branches" >
			<c:if test="${userList.branch == branches.id }">
				<td>${branches.name}</td>
			</c:if>
		</c:forEach>

		<c:forEach items="${positions}" var="positions" >
			<c:if test="${userList.position == positions.id }">
				<td>${positions.name}</td>
			</c:if>
		</c:forEach>

		<c:if test="${loginUser.id != userList.id }">
			<form method="get" action="isDeleted" >
			<input type="hidden" value="${userList.id}" name="userId">
		    <c:if test="${userList.isDeleted == 0}" >
		    	<td><button class="comment_btn" type="submit" value="${userList.isDeleted}" name="isDeleted"onclick="return confirm('本当に停止してもよろしいですか？');" >停止</button></td>
		    </c:if>
		    <c:if test="${userList.isDeleted == 1}" >
		    	<td><button class="comment_btn" type="submit" value="${userList.isDeleted}" name="isDeleted"onclick="return confirm('本当に復活してもよろしいですか？');">復活</button></td>
		    </c:if>
		    </form>
		    <form method="get" action="userSettings" >
		  		<td><button class="comment_btn" type="submit" value="${userList.id}" name="userId">編集</button></td>
		  	</form>
		</c:if>

	  	<c:if test="${loginUser.id == userList.id }">
	  		 <td> </td>
	  		<form method="get" action="userSettings" >
		  		<td><button class="comment_btn" type="submit" value="${userList.id}" name="userId">編集</button></td>
		  	</form>
	  	</c:if>
	   </tr>
</c:forEach>
</tbody>
</table>


<br/>
<p id="pageTop"><a href="./management">ページトップへ戻る</a></p>
<div class="copyright">Copyright(c)Yuki Tsuji</div>
</div>
</div>
</body>
</html>
