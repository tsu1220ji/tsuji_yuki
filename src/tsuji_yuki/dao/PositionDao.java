package tsuji_yuki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tsuji_yuki.beans.Position;
import tsuji_yuki.exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPositions(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM Positions ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositions(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Position> toPositions(ResultSet rs)throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Position Position = new Position();
				Position.setId(id);
				Position.setName(name);

				ret.add(Position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private void close(ResultSet rs) {
		// TODO 自動生成されたメソッド・スタブ

	}


	private void close(PreparedStatement ps) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
