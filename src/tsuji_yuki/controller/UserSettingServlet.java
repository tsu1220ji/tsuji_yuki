package tsuji_yuki.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.Branch;
import tsuji_yuki.beans.Position;
import tsuji_yuki.beans.User;
import tsuji_yuki.exception.NoRowsUpdatedRuntimeException;
import tsuji_yuki.service.BranchService;
import tsuji_yuki.service.PositionService;
import tsuji_yuki.service.UserService;


@WebServlet(urlPatterns = { "/userSettings" })
@MultipartConfig(maxFileSize = 100000)
public class UserSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

      	List<Branch> branches = new BranchService().getBranches();
    	List<Position> positions = new PositionService().getPositions();
    	session.setAttribute("branches" , branches);
    	session.setAttribute("positions" , positions);

    	if(StringUtils.isNumeric(request.getParameter("userId")) == false){
    		List<String> messages = new ArrayList<String>();
			messages.add("IDが正しくありません ");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./management");
			return;
    	}
    	if(StringUtils.isBlank(request.getParameter("userId"))){
    		List<String> messages = new ArrayList<String>();
			messages.add("IDが正しくありません ");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./management");
			return;
    	}
    	int userId = Integer.parseInt(request.getParameter("userId"));

    	User editUser = new UserService().getUser(userId);
    	if(editUser == null){
    		List<String> messages = new ArrayList<String>();
			messages.add("IDが正しくありません ");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./management");
			return;
    	}

    	request.setAttribute("editUser", editUser);
		request.setAttribute("userId", userId );
		String update = request.getParameter("updateDate");
		request.setAttribute("updateDate", update );
		request.getRequestDispatcher("userSettings.jsp").forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<String> messages = new ArrayList<String>();
		int userId = Integer.parseInt(request.getParameter("userId"));
		User editUser = getEditUser(request);

		User editUserOld = new UserService().getUser(userId);
		User user = (User) request.getSession().getAttribute("loginUser");

		List<String>userIdList = new ArrayList<String>();
		new UserService().idSelect(userIdList);

		//ログインIDが変更の有無
		if(editUser.getLoginId().equals(editUserOld.getLoginId()) == false){
			//ログインIDが重複した場合エラーメッセージ
			for(int i=0; i < userIdList.size(); i++){
				if( userIdList.get(i).equals(request.getParameter("loginId")) ){
					messages.add("ログインIDが重複しています");
					request.setAttribute("errorMessages", messages);
					request.setAttribute("positionsId", request.getParameter("position"));
					request.setAttribute("branchesId", request.getParameter("branch"));
					request.setAttribute("userId", userId );
					request.setAttribute("editUser", editUser);
					request.getRequestDispatcher("userSettings.jsp").forward(request,response);
					return;
				}
			}
		}


		if(user.getId() == editUser.getId()){
			if(editUserOld.getBranch().equals(request.getParameter("branch")) == false){
				messages.add("このアカウントの支店を変えることができません");
				request.setAttribute("errorMessages", messages);
				request.setAttribute("positionsId", request.getParameter("position"));
				request.setAttribute("branchesId", request.getParameter("branch"));
				request.setAttribute("userId", userId );
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("userSettings.jsp").forward(request,response);
				return;
			}
			if(editUserOld.getPosition().equals(request.getParameter("position")) == false){
				messages.add("このアカウントの役職を変えることができません");
				request.setAttribute("errorMessages", messages);
				request.setAttribute("positionsId", request.getParameter("position"));
				request.setAttribute("branchesId", request.getParameter("branch"));
				request.setAttribute("userId", userId );
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("userSettings.jsp").forward(request,response);
				return;
			}
		}



		 if(isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				request.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("userSettings.jsp").forward(request,response);
			}

			request.setAttribute("loginUser", editUser);
			request.removeAttribute("editUser");
			//session.setAttribute("loginUser", editUser);
			response.sendRedirect("./management");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("positionsId", request.getParameter("position"));
			request.setAttribute("branchesId", request.getParameter("branch"));
			request.setAttribute("userId", userId );
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("userSettings.jsp").forward(request,response);
		}
	}

	private User getEditUser(HttpServletRequest request)throws IOException, ServletException{

		User editUser = new User();

		int userIdInt = Integer.parseInt(request.getParameter("userId"));

		editUser.setId(userIdInt);
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setPosition(request.getParameter("position"));
		editUser.setBranch(request.getParameter("branch"));

		return editUser;
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String position = request.getParameter("position");
		String branch = request.getParameter("branch");
		String rePassword = request.getParameter("rePassword");


		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(StringUtils.length(loginId) > 0){
				if(StringUtils.length(loginId) < 6  ){
	    		messages.add("ログインIDを6文字以上にしてください");
	    	}
	    	if(StringUtils.length(loginId) > 20 ){
	    		messages.add("ログインIDを20文字以下にしてください");
	    	}
	    	if(StringUtils.isAsciiPrintable(loginId) == false){
	    		messages.add("ログインIDを半角英数字にしてください");
	    	}
		}

		if (StringUtils.isEmpty(password) == false) {
	    	if(StringUtils.length(password) < 6 ){
	    		messages.add("パスワードを6文字以上にしてください");
	    	}
	    	if(StringUtils.length(password) > 20 ){
	    		messages.add("パスワードを20文字以下にしてください");
	    	}
	    	if(StringUtils.isEmpty(rePassword)){
	    		messages.add("確認用パスワードを入力してください");
	    	}
    	}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(position) == true) {
			messages.add("役職を入力してください");
		}
		if (StringUtils.isEmpty(branch) == true) {
			messages.add("所属・支店を入力してください");
		}

    	if(StringUtils.length(rePassword) > 0 &&  StringUtils.length(password) > 0){
	    	if(!(password.equals(rePassword))){
				messages.add("パスワードと確認用パスワードがあってません");
			}
    	}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}