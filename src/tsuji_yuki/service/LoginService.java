package tsuji_yuki.service;

//import static tsuji_yuki.utils.CloseableUtil.*;
import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;

import tsuji_yuki.beans.User;
import tsuji_yuki.dao.UserDao;
import tsuji_yuki.utils.CipherUtil;


public class LoginService {

	public User login(String loginId, String password ) {


		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, loginId , encPassword );
			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
