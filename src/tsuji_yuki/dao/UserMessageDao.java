package tsuji_yuki.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.UserMessage;
import tsuji_yuki.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection,String categorySearch, String oldTimeSearch, String newTimeSearch) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();


			sql.append("SELECT * FROM user_message ");
			if(StringUtils.isEmpty(categorySearch) == false && StringUtils.isEmpty(newTimeSearch) == false && StringUtils.isEmpty(oldTimeSearch) == false){
				sql.append("WHERE category LIKE '%"+ categorySearch + "%'");
				sql.append("AND insert_date BETWEEN");
				sql.append("'" + oldTimeSearch + "'AND'" + newTimeSearch + " 23:59:59'");
			}
			if(StringUtils.isEmpty(categorySearch) == false && StringUtils.isEmpty(newTimeSearch) && StringUtils.isEmpty(oldTimeSearch)){
				sql.append("WHERE category LIKE '%"+ categorySearch + "%'");
			}
			if(StringUtils.isEmpty(categorySearch)&& StringUtils.isEmpty(newTimeSearch) == false && StringUtils.isEmpty(oldTimeSearch) == false){
				sql.append("WHERE insert_date BETWEEN");
				sql.append("'" + oldTimeSearch + "'AND'" + newTimeSearch + " 23:59:59'");
			}
			sql.append("ORDER BY insert_date DESC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private void close(PreparedStatement ps) {

	}
	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int userId = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setName(name);
				message.setUserId(userId);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setInsertDate(insertDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	private void close(ResultSet rs) {
	}
}
