package tsuji_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.Message;
import tsuji_yuki.beans.User;
import tsuji_yuki.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setSubject(request.getParameter("subject"));
			message.setUserId(user.getId());


			new MessageService().register(message);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("message" , request.getParameter("message"));
			session.setAttribute("category" , request.getParameter("category"));
			session.setAttribute("subject" , request.getParameter("subject"));
			response.sendRedirect("./message");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("message");
		String category = request.getParameter("category");
		String subject = request.getParameter("subject");



		if(StringUtils.isBlank(subject)){
			messages.add("件名を入力してください");
		}

		if(StringUtils.length(subject) > 0){
			if(subject.length() > 30){
				messages.add("件名を30文字以下にしてください");
			}

		}

		if(StringUtils.isBlank(message)){
			messages.add("本文を入力してください");
		}
		if (1000 < message.length()) {
			messages.add("本文を1000文字以下で入力してください");
		}

		if(StringUtils.isBlank(category)){
			messages.add("カテゴリーを入力してください");
		}
		if(category.length() > 10){
			messages.add("カテゴリーを10文字以下にしてください");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
