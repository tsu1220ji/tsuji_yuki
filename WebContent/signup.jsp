<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./CSS/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrap">
<div class = "main-contents">

<div class="header">
<a href="management"class="square_btn">ユーザー管理</a>
</div>
<br/>

<div class="background">
<c:if test="${ not empty errorMessages }">
	<div class="errormessage">
	<b>ERROR !!</b>
		<ul>
			<c:forEach items="${errorMessages }" var="message">
				<li><c:out value="${message}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<div class="title">
<p>ユーザー新規作成</p>
</div>
<form action="signup" method="post"><br/>

	<label for = "loginId">ログインID</label>　　　　　　
	<input name = "loginId" value = "${user.loginId}" id = "loginId "/><br/>
	<br/>
	<label for = "password">パスワード</label>　　　　　　
	<input name = "password" type = "password" id = "password"/><br/>
	<br/>
	<label for = "rePassword">パスワード(確認用)</label>　　
	<input name = "rePassword" type = "password" id = "rePassword"/><br/>
	<br/>
	<label for = "name">名前　　</label>　　　　　　　
	<input name = "name" value = "${user.name}" id = "name"/><br/>
	<br/>
	<label for = "branch">支店</label>　　　　　　　　　
	<select name="branch" id = "branch">
		<c:forEach items="${branches}" var="branches" >
			<c:if test="${branches.id == branchesId}">
				<option value="${branches.id}" selected>${branches.name}</option>
			</c:if>
			<c:if test="${branches.id != branchesId}">
			<option value="${branches.id}">${branches.name}</option>
			</c:if>
		</c:forEach>
	</select>
	<br/>	<br/>
	<label for = "position">役職</label>　　　　　　　　　
	<select name="position" id = "position">
		<c:forEach items="${positions}" var="positions" >
			<c:if test="${positions.id == positionsId}">
				<option value="${positions.id}" selected>${positions.name}</option>
			</c:if>
			<c:if test="${positions.id != positionsId}">
				<option value="${positions.id}">${positions.name}</option>
			</c:if>
		</c:forEach>
	</select>
	<br/>
	<br/>
	<input class="comment_btn" type="submit" value="登録" />
<c:remove var="user" scope="session"/>
<c:remove var="positionsId" scope="session"/>
<c:remove var="branchesId" scope="session"/>
<c:remove var="errorMessages" scope="session"/>
</form>
<div class = "copyright">Copyright (c)Yuki Tsuji</div>
</div>
</div>
</div>
</body>
</html>
