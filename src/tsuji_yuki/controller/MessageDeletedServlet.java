package tsuji_yuki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tsuji_yuki.service.MessageService;

@WebServlet("/messageDeleted")
public class MessageDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int messageId = Integer.parseInt(request.getParameter("messageId"));

		new MessageService().messageDeleted(messageId);
		response.sendRedirect("./");
	}
}
