package tsuji_yuki.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.User;
import tsuji_yuki.exception.NoRowsUpdatedRuntimeException;
import tsuji_yuki.service.UserService;


@WebServlet(urlPatterns = { "/settings" })
@MultipartConfig(maxFileSize = 100000)
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.sendRedirect("login.jsp");

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		User editUser = new UserService().getUser(loginUser.getId());
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("settings.jsp").forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<String> messages = new ArrayList<String>();

		User editUser = getEditUser(request);
		request.setAttribute("editUser", editUser);
		 if(isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("settings.jsp").forward(request,response);
			}
			request.setAttribute("loginUser", editUser);
			request.removeAttribute("editUser");

			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request,response);
		}
	}
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {


		User editUser = new User();

		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setPosition(request.getParameter("position"));
		editUser.setBranch(request.getParameter("branch"));

		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}