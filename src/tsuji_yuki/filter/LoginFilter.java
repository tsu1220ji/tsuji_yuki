package tsuji_yuki.filter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tsuji_yuki.beans.User;

@WebFilter(urlPatterns = { "/*"})
public class LoginFilter implements Filter{
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain){
    try{

    	HttpSession session = ((HttpServletRequest)request).getSession();
    	String requestPath = ((HttpServletRequest)request).getServletPath();

    	if(requestPath.equals("/CSS/login") == false){

	    	if(requestPath.equals("/login") == false){

	    		if(requestPath.equals("/CSS/style.css") == false){

		    		User user = (User) session.getAttribute("loginUser");

			    	if (user == null){
						List<String> messages = new ArrayList<String>();
						messages.add("ログインしてください ");
						request.setAttribute("errorMessages", messages);
			    		((HttpServletResponse)response).sendRedirect("./login");
			    		return;
			    	}
	    		}
		    }
    	}
	    chain.doFilter(request, response);

      }catch (ServletException se){
      }catch (IOException e){
    }
  }

  public void init(FilterConfig filterConfig) throws ServletException{
  }

  public void destroy(){
  }
}
