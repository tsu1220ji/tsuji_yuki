package tsuji_yuki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tsuji_yuki.beans.User;
import tsuji_yuki.exception.SQLRuntimeException;

public class ManagementDao {

	public List<User> getUserList(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users ");
			sql.append("ORDER BY branch_id ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private void close(PreparedStatement ps) {

	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		ArrayList<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String position = rs.getString("position_id");
				String branch = rs.getString("branch_id");
				Date updateDate = rs.getDate("update_date");
				int isDeleted = rs.getInt("is_deleted");

				User userList = new User();
				userList.setId(id);
				userList.setLoginId(loginId);
				userList.setName(name);
				userList.setPosition(position);
				userList.setBranch(branch);
				userList.setUpdateDate(updateDate);
				userList.setIsDeleted(isDeleted);

				ret.add(userList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private void close(ResultSet rs) {

	}
}