package tsuji_yuki.service;


import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tsuji_yuki.beans.Message;
import tsuji_yuki.beans.UserMessage;
import tsuji_yuki.dao.MessageDao;
import tsuji_yuki.dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}


	public List<UserMessage> getMessage(String categorySearch ,String oldTimeSearch ,String newTimeSearch) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection,categorySearch,oldTimeSearch,newTimeSearch);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void messageDeleted(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();



			MessageDao messageDao = new MessageDao();
			messageDao.messageDeleted(connection,messageId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
