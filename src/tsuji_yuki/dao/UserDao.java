package tsuji_yuki.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tsuji_yuki.beans.User;
import tsuji_yuki.exception.NoRowsUpdatedRuntimeException;
import tsuji_yuki.exception.SQLRuntimeException;


public class UserDao {
	public void insert(Connection connection , User user) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users(");
			sql.append("login_id,");
			sql.append("password,");
			sql.append("name,");
			sql.append("branch_id,");
			sql.append("position_id,");
			sql.append("is_deleted,");
			sql.append("update_date,");
			sql.append("insert_date");
			sql.append(")VALUES(");
			sql.append("?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,user.getLoginId());
			ps.setString(2,user.getPassword());
			ps.setString(3,user.getName());
			ps.setString(4,user.getBranch());
			ps.setString(5,user.getPosition());
			ps.setInt(6,user.getIsDeleted());

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	public User getUser(Connection connection,String loginId,String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? " ;

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch = rs.getString("branch_id");
				String position = rs.getString("position_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				int isDeleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setPosition(position);
				user.setBranch(branch);
				user.setInsertDate(insertDate);
				user.setUpdateDate(updateDate);
				user.setIsDeleted(isDeleted);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	public void update(Connection connection,User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			if(StringUtils.isNotBlank(user.getPassword())){
				sql.append(",password = ?");
			}
			sql.append(",name = ? ");
			sql.append(",position_id = ?");
			sql.append(",branch_id = ?");
			sql.append(" WHERE ");
			sql.append("id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			if(StringUtils.isNotBlank(user.getPassword())){
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setString(4, user.getPosition());
				ps.setString(5, user.getBranch());
				ps.setInt(6, user.getId());
			}else{
			ps.setString(2, user.getName());
			ps.setString(3, user.getPosition());
			ps.setString(4, user.getBranch());
			ps.setInt(5, user.getId());
			}
			int count = ps.executeUpdate();

			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}


	public User getUser (Connection connection, int id){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1,  id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {

				throw new SQLRuntimeException(e);
		} finally {
				close(ps);
		}

	}
	public void isDeletedUpdate(Connection connection , User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE ");
			sql.append(" id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsDeleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();

			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}

	}

	private void close(ResultSet rs) {
		// TODO 自動生成されたメソッド・スタブ

	}
	private void close(PreparedStatement ps) {
		// TODO 自動生成されたメソッド・スタブ

	}
	public void idSelect(Connection connection, List<String> userIdList) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT login_id FROM users";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String loginId = rs.getString("login_id");
				userIdList.add(loginId);
			}

		} catch (SQLException e) {

				throw new SQLRuntimeException(e);
		} finally {
				close(ps);
		}
	}
}