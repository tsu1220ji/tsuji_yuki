package tsuji_yuki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tsuji_yuki.beans.User;
import tsuji_yuki.service.UserService;

@WebServlet("/isDeleted")
public class IsDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int userId = Integer.parseInt(request.getParameter("userId"));
		int isDeleted = Integer.parseInt(request.getParameter("isDeleted"));

		if(isDeleted == 0){
			isDeleted = 1;
		}else{
			isDeleted = 0;
		}

		User user = new User();
		user.setId(userId);
		user.setIsDeleted(isDeleted);

		new UserService().isDeletedUpdate(user);

		response.sendRedirect("./management");

	}
}