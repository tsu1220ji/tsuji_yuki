package tsuji_yuki.service;

import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tsuji_yuki.beans.Position;
import tsuji_yuki.dao.PositionDao;


public class PositionService {

	public List<Position> getPositions() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> ret = positionDao.getPositions(connection);

			commit(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}
}