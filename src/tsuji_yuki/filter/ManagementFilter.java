package tsuji_yuki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tsuji_yuki.beans.User;


@WebFilter(urlPatterns = { "/management","/userSettings", "/signup"})
public class ManagementFilter implements Filter{
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException{
    try{
    	HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		if (loginUser == null){
			chain.doFilter(request, response);
			return;
		}

		String userPosition = loginUser.getPosition();

		if(	userPosition.equals("1")){
			chain.doFilter(request, response);
		}else{
			List<String> messages = new ArrayList<String>();
			messages.add("人事総務社員ではないので、アクセスが制限されています");
			session.setAttribute("errorMessages", messages);

			((HttpServletResponse)response).sendRedirect("./");
		}
      }catch (ServletException se){

    }
  }

  public void init(FilterConfig filterConfig) throws ServletException{
  }

  public void destroy(){
  }
}
