package tsuji_yuki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tsuji_yuki.beans.Branch;
import tsuji_yuki.beans.Position;
import tsuji_yuki.beans.User;
import tsuji_yuki.service.BranchService;
import tsuji_yuki.service.ManagementService;
import tsuji_yuki.service.PositionService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		List<User>userList = new ManagementService().getUserList();
     	List<Branch> branches = new BranchService().getBranches();
    	List<Position> positions = new PositionService().getPositions();
    	User user = (User) request.getSession().getAttribute("loginUser");

    	request.setAttribute("loginUser", user);

    	request.setAttribute("branches" , branches);
    	request.setAttribute("positions" , positions);
		request.setAttribute("userList", userList );
		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		request.getRequestDispatcher("/userSettings.jsp").forward(request, response);

	}
}
