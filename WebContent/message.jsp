<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム画面</title>
	<link href="./CSS/style.css" rel="stylesheet" type="text/css" >
</head>
<body>
<div id="wrap">

<div class="header">

	<c:if test="${ not empty loginUser }">
		<a href="./"class="square_btn">ホーム</a>
	</c:if>
<c:if test="${ not empty errorMessages }">
	<div class="errormessage">
		<b>ERROR !!</b>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<div class="new-message">

<div class="form-area">
		<form action="message" method="post">
			<div class="title">
				<p>掲示板投稿</p>
			</div>
			件名<input type="text" name="subject" class="textBox" value="${subject}">
			　　カテゴリー<input type="text" name="category" class="textBox" value="${category}">
			<br />
			<br />
			<textarea name="message" cols="120" rows="10"  class="textBox" >${message}</textarea>
			<br />
			（1000文字まで)<br />
			<br />
			<input class="comment_btn" type="submit" value="投稿">
		</form>
	<c:remove var="subject" scope="session"/>
	<c:remove var="category" scope="session"/>
	<c:remove var="message" scope="session"/>
	<div class="copyright">Copyright(c)Yuki Tsuji</div>
</div>
</div>
</div>
</div>
</body>
</html>