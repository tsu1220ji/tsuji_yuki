<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム画面</title>
	<link href="./CSS/style.css" rel="stylesheet" type="text/css" >
	<script type="text/javascript" src="./js/jQuery.min.js"></script>
</head>
<body>

<div class="main-contents">
<div id="wrap">
<div class="header">
	<c:if test="${ not empty loginUser }">
		<a href="message"class="square_btn">新規投稿</a>
		<c:if test="${ loginUser.position == 1 }">
			<a href="management"class="square_btn">ユーザー管理</a>
		</c:if>
		<a href="logout"class="square_btn">ログアウト</a>
<div class="searchs">
	<form action="./" method="get">
		<span class="box-title">検索</span><br />
		<p>
		カテゴリー<input type="text" name="categorySearch"  class="textBox" value="${categorySearch}">
		　　日時 <input type="date" name="oldTimeSearch" value="${oldTimeSearch}" >　～　<input type="date" name="newTimeSearch" value="${newTimeSearch}" >
		<input type="submit" value="検索" class="search_btn">
		<a href="./" class="reset_btn">リセット</a>
		</p>
	</form>
	</div>
	</c:if>
</div>

<c:if test="${ not empty errorMessages }">
<div class="errormessage">
	<div class="errorMessages ">
		<b>ERROR !!</b>
		<ul>
			<c:forEach items="${errorMessages }" var="message">
				<li><c:out value="${message}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</div>
</c:if>
<br />

<div class="messages">
	<c:forEach items="${messages}" var="message">
			<div class="message">
					<div class="subject"><h1><c:out value="${message.subject}" /></h1></div>
					<span class="message-name"><p><c:out value="${message.name}" />　</p></span><br/>
<%-- 					<div class="category"><u><c:out value="${message.category}" /></u>　　　</div> --%>
					<div class="category"><a href="./?categorySearch=${message.category}&oldTimeSearch=&newTimeSearch=">${message.category}</a></div>
					<div class="date"><a><fmt:formatDate value="${message.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></a></div>
					<br/>
					<br/>
					<div class="text">
					<p>
					<c:forEach var="s" items="${fn:split(message.text, '
					')}">
	    			<c:out value="${s}" /><br/>
					</c:forEach>
					</p>
					</div>
					<div class="messageDeleted">
					<form method="get" action="messageDeleted" >
						<c:if test="${loginUser.id == message.userId}" >
							<button class="delete_btn" type="submit" value="${message.id}" name="messageId" onclick="return confirm('本当に削除してもよろしいですか？');">投稿削除</button>
						</c:if>
					</form>
					</div>
					<br/>
				</div>

			<c:forEach items="${comments}" var="comment">

				<c:if test="${comment.messageId == message.id}" >
					<div class="comment-box">
					<p>
					<div class="name"><p><c:out value="${comment.name}" />　</p></div>
					<div class="date"><a><fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></a></div>
					<br/>
					<c:forEach var="s" items="${fn:split(comment.commentText, '
					')}">
    					<c:out value="${s}" /><br/>
					</c:forEach>
					<div class="comment-deleted">
						<form method="get" action="commentDeleted" >
							<c:if test="${loginUser.id == comment.userId}" >
								<button class="delete_btn" type="submit" value="${comment.id}" name="commentId" onclick="return confirm('本当にコメント削除してもよろしいですか？');">コメント削除</button>
							</c:if>
						</form>
					</div>
					</p>
					<br/>
					</div>
				</c:if>
			</c:forEach>
			<div class="comment-input">
			<form action="newComment" method="post">
				<input type="hidden" id="id" name="id" value="${message.id}">
				<c:if test="${messageIdComment == message.id}">
					<textarea name="comment" cols="90" rows="10" class="textBox">${commentText}</textarea>
				</c:if>

				<c:if test="${messageIdComment != message.id}">
					<textarea name="comment" cols="90" rows="8" class="textBox"></textarea><br />
				</c:if>
			<input class="comment_button" type="submit" value="コメント"><br />
			<br />
			</form>
			</div>
	</c:forEach>
	<c:remove var="commentText" scope="session"/>
</div>
<p id="pageTop"><a href="./">ページトップへ戻る</a></p>
<div class="copyright">Copyright(c)Yuki Tsuji</div>
</div>
</div>
</body>
</html>
