package tsuji_yuki.service;

import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tsuji_yuki.beans.Comment;
import tsuji_yuki.beans.UserComment;
import tsuji_yuki.dao.CommentDao;
import tsuji_yuki.dao.UserCommentDao;

public class CommentService {


	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}


	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void commentDeleted(int commentId) {


		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.commentDeleted(connection,commentId);

			commit(connection);
		} catch (RuntimeException e) {
		rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
