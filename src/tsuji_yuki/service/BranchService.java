package tsuji_yuki.service;

import static tsuji_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tsuji_yuki.beans.Branch;
import tsuji_yuki.dao.BranchDao;


public class BranchService {

	public List<Branch> getBranches() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranches(connection);

			commit(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ

	}
}