package tsuji_yuki.beans;

import java.io.Serializable;

public class Branch implements Serializable{
	private static final long serialVersionUID = 1L;

	private int Id;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
}
